#!/usr/bin/make -f

OBJECTS=socket-activate.1 python/NEWS python/README.md

all: $(OBJECTS)

socket-activate.1: socket-activate.md
	pandoc -s -t man -o $@ $<

python/NEWS: NEWS
	cp NEWS python/NEWS

python/README.md: socket-activate.md
	cp socket-activate.md python/README.md

clean:
	rm -f $(OBJECTS) tests/expected tests/produced
	find tests/ -type s -name 'sock*' -delete
	rm -rf \
		python/.mypy_cache \
		python/.tox \
		python/build \
		python/dist \
		python/socket_activate.egg-info \
		python/socket_activate/__pycache__/ \
		python/unit_tests/__pycache__/

check: check-python

check-python:
	SOCKET_ACTIVATE=$(CURDIR)/python/scripts/socket-activate.sh ./tests/basic
	SOCKET_ACTIVATE=$(CURDIR)/python/scripts/socket-activate.sh ./tests/unix
	SOCKET_ACTIVATE=$(CURDIR)/python/scripts/socket-activate.sh ./tests/inet

VERSION ?= $(shell awk '/^version/{ print $$2 }' < NEWS | head -n1)

release:
	git tag -s socket-activate_$(VERSION) -m 'Tagging socket-activate version $(VERSION)' master

.PHONY: clean all check check-python release
