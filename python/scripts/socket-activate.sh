#!/bin/sh

set -e

PYTHONPATH="$(dirname -- "$(dirname -- "$0")")" exec python3 -m socket_activate "$@"
