#!/usr/bin/python3
""" Open sockets and `exec` a socket-activated service.

Author: Daniel Kahn Gillmor <dkg@fifthhorseman.net>
Copyright: February 2019
License: GPLv3+ (see https://www.gnu.org/licenses/gpl.html) """

import collections
import os
import sys
import logging
import ipaddress
import socket
import select

from typing import Dict, List, Tuple

# log: a logger
# keepers: sockets that need to avoid being destroyed before the exec()
State = collections.namedtuple('State', (
    'log',
    'keepers',
))


def initstate(argv: List[str]) -> State:
    """ Initialize the global state array - mainly the logger. """
    handler = logging.StreamHandler(sys.stderr)
    handler.setLevel(logging.DEBUG)
    logger = logging.getLogger(argv[0])
    logger.addHandler(handler)

    return State(
        log=logger,
        keepers=[],
    )


def connectinet(state: State,
                options: Dict[str, str],
                address: str,
                socktype: socket.SocketKind,  # pylint: disable=no-member
                ) -> Tuple[socket.SocketType, Dict[str, str]]:
    """ Set up a TCP or UDP listening socket. """
    if '/' in address:
        (saddr, sport) = address.split('/', maxsplit=1)
        port = int(sport)
        addr = ipaddress.ip_address(saddr)
        if addr.version == 4:
            family = socket.AF_INET
        elif addr.version == 6:
            family = socket.AF_INET6
        else:
            state.log.warning(f'could not determine address family '
                              f'for {address}')
            sys.exit(100)
    else:
        port = int(address)
        family = socket.AF_INET
        addr = ''

    sock = socket.socket(family=family, type=socktype)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
    sock.bind((str(addr), port))
    return (sock, options)


def connectunix(_state: State,
                options: Dict[str, str],
                address: str,
                socktype: socket.SocketKind,  # pylint: disable=no-member
                ) -> Tuple[socket.SocketType, Dict[str, str]]:
    """ Set up a Unix-domain listening socket. """
    sock = socket.socket(family=socket.AF_UNIX, type=socktype)
    if 'mode' in options:
        mode = int(options['mode'], base=8)
        oldumask = os.umask(0o0777 & mode)
    sock.bind(address)
    if 'mode' in options:
        os.umask(oldumask)
        os.chmod(sock.fileno(), mode)

    if 'user' in options or 'group' in options:
        suser = options.get('user', None)
        if suser is None:
            user = os.getuid()
        else:
            user = int(suser)

        sgroup = options.get('group', None)
        if sgroup is None:
            group = os.getgid()
        else:
            group = int(sgroup)
        os.chown(sock.fileno(), user, group)

    # drop unix-domain-specific options before returning them
    for opt in ['user', 'group', 'mode']:
        if opt in options:
            del options[opt]
    return (sock, options)


def addlistener(state: State,
                fdesc: int,
                family: str,
                options: Dict[str, str],
                address: str
                ) -> str:
    """ Parse a single listen specification, set up the file descriptor. """
    state.log.debug(f'trying to listen for FD {fdesc} on {family}, '
                    f'address: {address}, options: {options}')

    handlers = {
        'udp': (connectinet, socket.SOCK_DGRAM),
        'tcp': (connectinet, socket.SOCK_STREAM),
        'unix': (connectunix, socket.SOCK_STREAM),
        'unix-dgram': (connectunix, socket.SOCK_DGRAM),
    }
    handler = handlers.get(family)
    if handler is None:
        state.log.warning(f'unknown family: {family}')
        sys.exit(100)
    socktype = handler[1]
    (newsocket, options) = handler[0](state, options, address, socktype)
    if socktype == socket.SOCK_STREAM:
        backlog = options.get('backlog', socket.SOMAXCONN)
        if 'backlog' in options:
            del options['backlog']
        newsocket.listen(int(backlog))

    unknown_opts = set(options.keys()).difference(set(['label']))
    if unknown_opts:
        oname = 'option'
        if len(unknown_opts) > 1:
            oname = 'options'
        state.log.warning(f'unknown {oname} for family {family}: '
                          f'{unknown_opts}')
        sys.exit(100)

    if newsocket.fileno() == fdesc:
        # avoid having the socket object get closed by python's
        # garbage collection:
        state.keepers.append(newsocket)
        os.set_inheritable(newsocket.fileno(), True)
    else:
        state.log.debug(f'moving new FD {newsocket.fileno()} to {fdesc}')
        os.dup2(newsocket.fileno(), fdesc)

    return options.get('label', '')


def parse_args(state: State, args: List[str]) -> Tuple[List[str], List[str]]:
    """ Parse the command-line arguments, invoke addlisteners(). """
    listeners = []
    nextfd = 3
    while args:
        arg = args.pop(0)
        state.log.debug(f'parsing arg {arg}')
        if arg in ['-v', '--verbose']:
            state.log.setLevel(logging.DEBUG)
        elif arg == '--':
            break
        elif arg[0] != '-':
            args.insert(0, arg)
            break
        elif arg.startswith('--'):
            (family, opts, address) = arg[2:].split(':', maxsplit=2)
            options: Dict[str, str] = {}
            for opt in filter((lambda x: x != ''), opts.split(',')):
                try:
                    key, value = opt.split('=', maxsplit=1)
                except ValueError:
                    state.log.warning(f'option has no value: {opt}')
                    sys.exit(100)
                if key in options:
                    state.log.warning(f'repeated option: {key}')
                    sys.exit(100)
                options[key] = value
            listeners.append(addlistener(state, nextfd, family,
                                         options, address))
            nextfd += 1
        else:
            state.log.warning(f'unknown option: {arg}')
            sys.exit(100)
    if not args:
        state.log.warning('no program specified')
        sys.exit(100)
    return (listeners, args)


def main() -> None:
    """ Main program: set up, wait for a connection, execute the program. """
    state = initstate(sys.argv)
    (listeners, args) = parse_args(state, sys.argv[1:])
    os.environ['LISTEN_PID'] = str(os.getpid())
    os.environ['LISTEN_FDS'] = str(len(listeners))
    os.environ['LISTEN_FDNAMES'] = ':'.join(listeners)
    # wait until something happens to do the exec:
    dname = 'descriptor'
    if len(listeners) > 1:
        dname = 'descriptors'
    state.log.debug(f'select()ing for some inbound activity on '
                    f'{len(listeners)} file {dname} before exec()ing...')
    fds = list(range(3, 3+len(listeners)))
    (fds_r, fds_w, fds_x) = select.select(fds, [], fds)
    state.log.debug(f'saw activity on r={fds_r}, w={fds_w}, x={fds_x}')
    state.log.debug(f'executing {args}')
    os.execvp(args[0], args)


if __name__ == '__main__':
    main()
