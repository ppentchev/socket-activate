#!/usr/bin/python3

""" A couple of simple tests for the socket_activate library. """

import itertools
import logging
import socket
import unittest

from typing import Dict, List, Tuple

from mypy_extensions import TypedDict

import ddt  # type: ignore
import mock
import pytest  # type: ignore

from socket_activate import __main__ as testee


ConnectTestData = TypedDict('ConnectTestData', {
    'arg': str,
    'address': str,
    'port': int,
    'family': socket.AddressFamily,  # pylint: disable=no-member
})

ListenerTestData = TypedDict('ListenerTestData', {
    'family': str,
    'inet': bool,
    'datagram': bool,
})

ParseTestData = TypedDict('ParseTestData', {
    'args': List[str],
    'args_out': List[str],
    'error': bool,
    'listeners': Tuple[int, str, Dict[str, str], str],
})


@ddt.ddt
class TestBasic(unittest.TestCase):
    """ Test some basic features. """
    # pylint: disable=no-self-use

    def test_initstate(self) -> None:
        """ Test the initial program state. """
        state = testee.initstate(['socket-activate'])

        assert isinstance(state.log, logging.Logger)
        assert state.log.getEffectiveLevel() == logging.WARNING
        assert len(state.log.handlers) == 1
        assert state.log.handlers[0].level == logging.DEBUG

        assert state.keepers == []

    @mock.patch('ipaddress.ip_address', autospec=True, spec_set=True)
    @mock.patch('socket.socket', autospec=True, spec_set=True)
    def test_connectinet_bad(self,
                             mock_socket: mock.MagicMock,
                             mock_ip_address: mock.MagicMock
                             ) -> None:
        """ Test connectinet() failure with an unsupported address family. """
        ads = mock.Mock()
        ads.version = 42
        mock_ip_address.return_value = ads

        state = mock.NonCallableMock(spec_set=['log', 'keepers'])

        with pytest.raises(SystemExit):
            testee.connectinet(state, {}, 'foo/80', socket.SOCK_STREAM)

        mock_ip_address.assert_called_once_with('foo')
        assert state.log.warning.call_count == 1
        assert mock_socket.call_count == 0

    @mock.patch('socket.socket', autospec=True, spec_set=True)
    @ddt.data(
        {
            'arg': '10.6.1.6/616',
            'address': '10.6.1.6',
            'port': 616,
            'family': socket.AF_INET,
        },
        {
            'arg': '42',
            'address': '',
            'port': 42,
            'family': socket.AF_INET,
        },
        {
            'arg': '::10:6:1:6/616',
            'address': '::10:6:1:6',
            'port': 616,
            'family': socket.AF_INET6,
        },
    )
    def test_connectinet(self,
                         data: ConnectTestData,
                         mock_socket: mock.MagicMock,
                         ) -> None:
        """ Test the way connectinet() sets up a listening socket. """
        options = {'sentinel': 'options'}
        state = mock.NonCallableMock(spec_set=['log', 'keepers'])
        socket_res = mock.NonCallableMock(spec_set=['bind', 'setsockopt'])

        mock_socket.return_value = socket_res

        res = testee.connectinet(state, options, data['arg'],
                                 socket.SOCK_DGRAM)

        mock_socket.assert_called_once_with(family=data['family'],
                                            type=socket.SOCK_DGRAM)
        socket_res.bind.assert_called_once_with((data['address'],
                                                 data['port'],))

        assert len(res) == 2
        assert res[0] is socket_res
        assert res[1] is options

    @mock.patch('socket.socket', autospec=True, spec_set=True)
    @mock.patch('os.umask', autospec=True, spec_set=True)
    @mock.patch('os.chmod', autospec=True, spec_set=True)
    @mock.patch('os.chown', autospec=True, spec_set=True)
    @mock.patch('os.getuid', autospec=True, spec_set=True)
    @mock.patch('os.getgid', autospec=True, spec_set=True)
    @ddt.data(*itertools.product(
        (False, True),  # pass_mode
        (False, True),  # pass_user
        (False, True),  # pass_group
    ))
    @ddt.unpack
    def test_connectunix(self,
                         pass_mode: bool,
                         pass_user: bool,
                         pass_group: bool,
                         mock_getgid: mock.MagicMock,
                         mock_getuid: mock.MagicMock,
                         mock_chown: mock.MagicMock,
                         mock_chmod: mock.MagicMock,
                         _mock_umask: mock.MagicMock,
                         mock_socket: mock.MagicMock,
                         ) -> None:
        # pylint: disable=too-many-arguments
        """ Test the way connectunix() sets up a listening socket. """
        state = testee.State(log=logging.getLogger('test'), keepers=[])

        fileno_res = {'sentinel': 'fileno'}
        socket_res = mock.NonCallableMock(spec_set=['fileno', 'bind'])
        socket_res.fileno.return_value = fileno_res
        mock_socket.return_value = socket_res

        mock_getuid.return_value = 616
        mock_getgid.return_value = 42

        options = {'something': 'else'}
        if pass_mode:
            options['mode'] = '125'
        if pass_user:
            options['user'] = '617'
        if pass_group:
            options['group'] = '43'

        res = testee.connectunix(state, options, '/path/to/socket',
                                 socket.SOCK_STREAM)

        mock_socket.assert_called_once_with(family=socket.AF_UNIX,
                                            type=socket.SOCK_STREAM)
        socket_res.bind.assert_called_once_with('/path/to/socket')

        if pass_mode:
            mock_chmod.assert_called_once_with(fileno_res, 0o125)
        else:
            assert mock_chmod.call_count == 0

        if pass_user:
            assert mock_getuid.call_count == 0
            if pass_group:
                assert mock_getgid.call_count == 0
                mock_chown.assert_called_once_with(fileno_res, 617, 43)
            else:
                mock_getgid.assert_called_with()
                mock_chown.assert_called_once_with(fileno_res, 617, 42)

        if pass_group:
            assert mock_getgid.call_count == 0
            if pass_user:
                assert mock_getuid.call_count == 0
                mock_chown.assert_called_once_with(fileno_res, 617, 43)
            else:
                mock_getuid.assert_called_with()
                mock_chown.assert_called_once_with(fileno_res, 616, 43)

        if not pass_user and not pass_group:
            assert mock_chown.call_count == 0

        assert len(res) == 2
        assert res[0] is socket_res
        assert res[1] is options

        assert options == {'something': 'else'}

    @mock.patch('os.dup2', autospec=True, spec_set=True)
    @mock.patch('os.set_inheritable', autospec=True, spec_set=True)
    @mock.patch('socket_activate.__main__.connectinet',
                autospec=True, spec_set=True)
    @mock.patch('socket_activate.__main__.connectunix',
                autospec=True, spec_set=True)
    @ddt.data(*itertools.product([
        {
            'family': 'tcp',
            'inet': True,
            'datagram': False,
        },
        {
            'family': 'udp',
            'inet': True,
            'datagram': True,
        },
        {
            'family': 'unix',
            'inet': False,
            'datagram': False,
        },
        {
            'family': 'unix-dgram',
            'inet': False,
            'datagram': True,
        },
    ], (False, True)))
    @ddt.unpack
    def test_addlistener(self,
                         data: ListenerTestData,
                         same: bool,
                         mock_unix: mock.MagicMock,
                         mock_inet: mock.MagicMock,
                         mock_set_inheritable: mock.MagicMock,
                         mock_dup2: mock.MagicMock,
                         ) -> None:
        # pylint: disable=too-many-arguments
        """ Test the creation of a listener. """
        options = {'label': 'whee'}
        address = 'somewhere far beyond'
        state = testee.State(
            log=mock.NonCallableMock(spec_set=['debug']),
            keepers=[],
        )

        newsocket = mock.NonCallableMock(spec_set=['fileno', 'listen'])
        newsocket.fileno.return_value = 42 if same else 43
        socktype = socket.SOCK_DGRAM if data['datagram'] \
            else socket.SOCK_STREAM
        mock_inet.return_value = (newsocket, options)
        mock_unix.return_value = (newsocket, options)

        res = testee.addlistener(state, 42, data['family'], options, address)

        def assert_connect(called: mock.MagicMock,
                           not_called: mock.MagicMock,
                           ) -> None:
            """ Assert the correct connect*() function was called. """
            assert not_called.call_count == 0
            called.assert_called_with(state, options, address, socktype)

        if data['inet']:
            assert_connect(mock_inet, mock_unix)
        else:
            assert_connect(mock_unix, mock_inet)

        if same:
            assert mock_dup2.call_count == 0
            mock_set_inheritable.assert_called_with(42, True)
            assert state.keepers == [newsocket]
        else:
            mock_dup2.assert_called_with(43, 42)
            assert mock_set_inheritable.call_count == 0
            assert state.keepers == []

        assert res == 'whee'

    @mock.patch('socket_activate.__main__.addlistener',
                autospec=True, spec_set=True)
    @ddt.data(
        {
            'args': ['-v', '--unix:label=one:/path', 'env'],
            'args_out': ['env'],
            'error': False,

            'listeners': [
                (3, 'unix', {'label': 'one'}, '/path'),
            ],
        },
        {
            'args': ['-v', '--unix:label=two:/path', '--', 'env'],
            'args_out': ['env'],
            'error': False,

            'listeners': [
                (3, 'unix', {'label': 'two'}, '/path'),
            ],
        },
        {
            'args': ['--unix:label=three,letter=a:/path', '--', 'date', '-R'],
            'args_out': ['date', '-R'],
            'error': False,

            'listeners': [
                (3, 'unix', {'label': 'three', 'letter': 'a'}, '/path'),
            ],
        },
        {
            'args': [
                '--tcp:label=four:80',
                '--udp:label=and more:127.0.0.1/19',
                '--', 'date', '-R',
            ],
            'args_out': ['date', '-R'],
            'error': False,

            'listeners': [
                (3, 'tcp', {'label': 'four'}, '80'),
                (4, 'udp', {'label': 'and more'}, '127.0.0.1/19'),
            ],
        },
        {
            'args': ['--tcp:label=novalue,opt:80', '--', 'date', '-R'],
            'args_out': ['date', '-R'],
            'error': True,
        },
        {
            'args': ['--tcp:label=too,label=many:80', '--', 'date', '-R'],
            'args_out': ['date', '-R'],
            'error': True,
        },
        {
            'args': ['-v', '--unix:label=one:/path'],
            'args_out': [],
            'error': True,
        },
        {
            'args': ['-v', '--unix:label=one:/path', '-X', '--', 'env'],
            'args_out': ['--', 'env'],
            'error': True,
        },
    )
    def test_parse_args(self,
                        data: ParseTestData,
                        mock_add: mock.MagicMock,
                        ) -> None:
        """ Test the parse_args() parsing of command-line options. """
        logger = mock.Mock(name='logger',
                           spec_set=['debug', 'setLevel', 'warning'])
        state = testee.State(log=logger, keepers=[])

        def do_addlistener(a_state: testee.State,
                           nextfd: int,
                           family: str,
                           options: Dict[str, str],
                           address: str,
                           ) -> Tuple[int, str, Dict[str, str], str]:
            """ Simulate an addlistener() call. """
            assert a_state is state
            return (nextfd, family, options, address)

        mock_add.side_effect = do_addlistener

        args = list(data['args'])
        if data['error']:
            with pytest.raises(SystemExit):
                testee.parse_args(state, args)
        else:
            listeners, args_out = testee.parse_args(state, args)

        if '-v' in data['args'] or '--verbose' in data['args']:
            logger.setLevel.assert_called_once_with(logging.DEBUG)
        else:
            assert logger.setLevel.call_count == 0

        if data['error']:
            assert logger.warning.call_count == 1
            return

        assert logger.warning.call_count == 0

        assert args is args_out
        assert args_out == data['args_out']
        assert listeners == data['listeners']
